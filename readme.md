# Задание 1


Был создан новый репозиторий и сделан изначальный коммит.

```
mkdir repo/
cd repo
git init
touch readme.md
git add readme.md
git commit
```

Создал и перешел в новую ветку.

```
git branch task_1
git checkout task_1
```

Добавил отчет (в виде readme.md) в новую ветку и отправил изменения на сервер.

```
git add .
git commit -m "Added a readme to this branch"

git remote add repo https://gitlab.com/uni_unorganized/repo
git push repo task_1
```